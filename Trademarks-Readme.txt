Trademark guidelines 
====================

You are welcome to use the Ubuntu-title font but if you are making logos for
software products, you should be mindful of Ubuntu's trademarks.

The Ubuntu trademarks are in the word "ubuntu" and somewhat also in the design
of the Ubuntu logo.  

The objective of the Ubuntu trademark policy is to encourage widespread use of
the Ubuntu trademarks by the Ubuntu community while controlling that use in
order to avoid confusion on the part of Ubuntu users and the general public, to
maintain the value of the image and reputation of the trademarks and to protect
them from inappropriate or unauthorised use.

The Ubuntu trademark policy is available on:
http://www.ubuntu.com/aboutus/trademarkpolicy

If you have concerns about your use of the font you are welcome to contact
trademarks@canonical.com for clarification.  It will probably not be an issue
unless you are creating a logo for a software product.


